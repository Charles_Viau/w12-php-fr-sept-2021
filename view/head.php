<!DOCTYPE html>
<html lang="fr-CA">
<?php
if (!isset($index_loaded)) {
    header('HTTP/1.0 403 acces direct a ce fichier est interdit');
    die('acces direct a ce fichier est interdis');
}
?>

<head>
    <meta charset="UTF-8">
    <title><?= $page_data['titre']; ?></title>
    <meta name="DESCRIPTION" content=" <?= $page_data['desc']; ?>">
    <meta name="author" content="<? config::PAGE_AUTHOR; ?>">
    <!-- web site icon -->
    <LINK REL="icon" href="<?= config::WEBSITE_ICON; ?>">

    <!--css-->
    <link rel="stylesheet" href="css/global.css">
    <!--bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <!--java-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <!--IMPORTANT pour responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>