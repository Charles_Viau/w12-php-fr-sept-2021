<?php

if (!isset($index_loaded)) {
    header('HTTP/1.0 403 acces direct a ce fichier est interdit');
    die('acces direct a ce fichier est interdis');
}

class webpage
{
    public static function render($page_data)
    {
        global $index_loaded;
        require_once "head.php";
        require_once "header.php";
        require_once "nav.php";
        echo $page_data['contenu'];
        require_once "footer.php";
    }

    public static function create_page_data($title, $description, $content)
    {
        $page_data['titre'] = $title;
        $page_data['desc'] = $description;
        $page_data['contenu'] = $content;

        return $page_data;
    }
}
