<?php

class users
{
    public function __construct()
    {
    }
    public function __destruct()
    {
    }
    public function login($msg = '')
    {
        $page_data['titre'] = 'connectez-vous';
        $page_data['desc'] = 'connectez-vous pour voir vos commande';
        $page_data['contenu'] = $msg;
        $page_data['contenu'] .= <<<HTML
    <form action="index.php" method="POST">
        <input type="hidden" name="op" value="6"><br>
        <input type="email" name="email" required maxlength=126><br>
        <input type="password" name="pw" required maxlength=8><br>
        <input type="submit" value="Connectez">
    </form>
HTML;
        webpage::render($page_data);
    }

    public function login_verify()
    {
        if (!isset($_SESSION['loginCount'])) {
            $_SESSION['loginCount'] = 0;
        } else {
            $_SESSION['loginCount']++;
        }

        if ($_SESSION['loginCount'] < 3) {
            //validate email
            $err_msg = '';


            if (!isset($_REQUEST['email'])) {
                crash(500, 'Email non recu');
            } else {
                $email = $_REQUEST['email'];
                if (strlen($email) > 126) {
                    $err_msg .= 'email trop long, max 126 caracteres';
                }

                $email = htmlspecialchars($email);

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $err_msg .= 'email invalide';
                }
            }

            // validate password
            if (!isset($_REQUEST['pw'])) {
                crash(500, 'password non recu');
            } else {
                $pw = $_REQUEST['pw'];
                if (strlen($pw) > 8) {
                    $err_msg .= 'password trop long, max 8 caracteres';
                }

                $pw = htmlspecialchars($pw);

                //regex or password filter if necessary.
            }

            if ($err_msg !== '') {
                $this->login($err_msg);
            } else {
                //look for account
                $DB = new db_pdo();
                $users = $DB->query_select("SELECT * FROM users WHERE email = '$email'");
                if (count($users) == 1 && password_verify($pw, $users[0]['pw'])) {
                    $_SESSION['email'] = $email;
                    $_SESSION['user_pic'] = $users[0]['picture'];
                    $_SESSION['level'] = $users[0]['level'];
                    setcookie('last_login_timestamp', time(), time() + (2 * 60 * 60 * 24 * 365));
                    $page_data['titre'] = 'Vous etes Connecte!';
                    $page_data['desc'] = 'Connexion reussie';
                    $page_data['contenu'] = <<<HTML
                        <h2>Vous etes connecter</h2>
HTML;
                    $_SESSION['loginCount'] = 0;
                    webpage::render($page_data);
                }
                $this->login('Email ou mot de passe invalide');
            }
        } else {
            $page_data['titre'] = 'plus de tentatives!';
            $page_data['desc'] = 'Connexion interdite';
            $page_data['contenu'] = <<<HTML
            <h2>Reesayez plus tard</h2>
HTML;
            webpage::render($page_data);
        }
    }

    public function logout()
    {
        session_unset();
        header('location:index.php');
    }

    public function register($msg = "")
    {
        $DB = new db_pdo();
        $provinces = $DB->query_select('SELECT * FROM provinces');
        $provinceHTML = tools::create_html_select($provinces, 'province', true);
        $pays = $DB->query_select('SELECT * FROM countries');
        $paysHTML = tools::create_html_select($pays, 'country', true);

        $page_data['contenu'] = $msg;
        $page_data['contenu'] .= <<<HTML
        <form enctype="multipart/form-data" action="index.php" method="POST">
            <input type="hidden" name="op" value="8">
            <input type="text" name="fullname" maxlength="50" autofocus placeholder='Name' required><br>
            <input type="text" name="adress" maxlength="255" placeholder="Adress"><br>
            <input type="text" name="city" maxlength="50" placeholder="City"><br>
            {$provinceHTML}<br>
            {$paysHTML}<br>
            <input type="text" name="postal_code" maxlength="7" placeholder="Postal Code"><br>
            <label for="francais">Francais</label>
            <input type="radio" name="language" id="francais" value="fr"><br>
            <label for="anglais">Anglais</label>
            <input type="radio" name="language" id="anglais" value="en"><br>
            <label for="other">Other</label>
            <input type="radio" name="language" id="other" value="autre" />
            <input type="text" name='other_lang' maxlength=25><br>
            <input type="email" name='email' maxlength="126" required placeholder="Email"><br>
            <input type="password" name="pw" placeholder="Password" maxlength="8"><br>
            <input type="password" name="pw2" placeholder="Confirm Password" maxlength="8"><br>
            <label for="spam_ok">Je desire recevoir les nouvelles</label>
            <input type="checkbox" name="spam_ok" checked value="1" id="spam_ok"><br>
            <label for="file">Choisir une image</label>
            <input type="file" name="fichier" id="file"><br>
            <input type='submit' value='continue'>
        </form>
HTML;
        $page_data['titre'] = 'Inscrivez-vous';
        $page_data['desc'] = 'Creer votre compte pour magasiner';
        webpage::render($page_data);
    }

    public function register_verify()
    {
        $err_msg = '';

        $err_msg .= tools::Picture_Uploaded_Save_File('fichier', 'user_img');

        if (!isset($_REQUEST['email'])) {
            crash(500, 'Email non recu');
        } else {
            $email = $_REQUEST['email'];
            if (strlen($email) > 126) {
                $err_msg .= 'email trop long, max 126 caracteres';
            }

            $email = htmlspecialchars($email);
        }

        //pw et p2 identique?

        //TODO all input verify
        if ($err_msg === '') {
            //Encoder pw
            $pw_encode = password_hash($_POST['pw'], PASSWORD_DEFAULT);

            //$sql = "INSERT INTO users (email, pw, fullname, level, language) VALUE(" . $_POST['email'] . ", " . $_POST['pw_encode'] . ", " . $_POST['fullname'] . ", client " . $_POST['language'] . ")";
            $sql = "INSERT INTO users (email, pw, fullname, level, language, picture) VALUE(?,?,?,?,?,?)";
            $pic_name = basename($_FILES['fichier']['name']);
            $params = [$_POST['email'], $pw_encode, $_POST['fullname'], 'client', $_POST['language'], $pic_name];
            $DB = new db_pdo();
            $DB->query_params($sql, $params);

            $page_data['titre'] = 'Vous etes inscrits';
            $page_data['desc'] = 'Inscription reussi';
            $page_data['contenu'] = '<h2>vous etes inscrit</h2>';
            webpage::render($page_data);
        } else {
            $this->register($err_msg, $_POST);
        }
    }

    public static function isLogAsAdmin()
    {
        if (!isset($_SESSION['email'])) {
            return false;
        }
        return ($_SESSION['level'] === 'admin');
    }
}
