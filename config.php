<?php
if (!isset($index_loaded)) {
    header('HTTP/1.0 403 acces direct a ce fichier est interdit');
    die('acces direct a ce fichier est interdis');
}
/**
 * System configuration
 */
class config
{
    const COMPANY_NAME = 'Classic Models Inc.';
    const COMPANY_EMAIL = 'email@email.com';
    const COMPANY_PHONE = '514-123-4567';
    const COMPANY_ADRESS = '123 rue principale, QC, Canada, H1V 2V1';

    const WEBSITE_ICON = 'website_icon.jpg';
    const PAGE_TITLE_DEFAULT = 'Reduced Model to Sell At Classic Models Inc.';
    const PAGE_DESCRIPTION_DEFAULT = 'Model selection between car, bus, plane, boat and more.';
    const PAGE_AUTHOR = 'Charles Viau';
}
