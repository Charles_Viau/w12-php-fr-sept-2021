<?php
class customers
{
    public function list()
    {
        $DB = new db_pdo();

        if (isset($_POST['customerNumber']) && $_POST['customerNumber'] != '') {
            $customers = $DB->query_select_params("SELECT * FROM customers WHERE customerNumber = ?", [$_POST['customerNumber']]);
        } else {
            $customers = $DB->query_select("SELECT * FROM customers");
        }
        $html = "<h2>Customers</h2>";
        $html .= "<form action='index.php?op=200' method='POST'>";
        $html .= "Entrez no. Client";
        $html .= "<input type='text' name='customerNumber'>";
        $html .= "<input type='submit' value='Research'>";
        $html .= "</form>";
        $html .= "<table>";
        $html .= "<tr><th>number</th><th>name</th><th>phone</th></tr>";
        foreach ($customers as $one_customer) {
            $html .= '.<tr><td>' . $one_customer['customerNumber'] . '</td><td>' . $one_customer['customerName'] . '</td><td>' . $one_customer['phone'] . '</td></tr>';
        }
        $html .= '</table>';
        $page_data['titre'] = 'Liste de clients';
        $page_data['desc'] = 'Liste de clients - resultat de la recherche';
        $page_data['contenu'] = $html;

        webpage::render($page_data);
    }

    public function listJSON()
    {
        $DB = new db_pdo();
        $customers = $DB->query_select("SELECT * FROM customers");
        $customersJSON = json_encode($customers, JSON_PRETTY_PRINT);
        header("Content-Type:application/json");
        echo $customersJSON;
    }
}
