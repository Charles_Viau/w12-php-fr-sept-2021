<?php
session_start();
$index_loaded = true; //etempe entree au bonne endroit
require_once "config.php";
require_once "outils.php";
require_once "view/webpage.php";
require_once "users.php";
require_once "db_pdo.php";
require_once "customers.php";
require_once "product-line.php";

/**
 * fonction principal
 */


function main()
{
    if (isset($_REQUEST['op'])) {
        $op = $_REQUEST['op'];
    } else {
        $op = 0;
    }

    switch ($op) {
        case 0:
            $compte = view_count("accueil_count.txt");
            $page_data = [
                'titre' => 'Acceuil-' . config::COMPANY_NAME,
                'desc' => config::PAGE_DESCRIPTION_DEFAULT,
                'contenu' => '<p>Bienvenue chez nous<p>
                Nb de vue =' . $compte . '<br>Derniere visite : ' . $_COOKIE['last_login_timestamp']
            ];
            view_count("accueil_count.txt");
            webpage::render($page_data);
            break;

        case 1:
            //about
            $page_data = [
                'titre' => 'A propos de nous' . config::COMPANY_NAME,
                'desc' => 'Nous sommes les specialiste',
                'contenu' => '<h1>Bienvenue chez nous</h1>'
            ];
            view_count("about_count.txt");
            webpage::render($page_data);
            break;
        case 5:
            //afficher formulaire de connexion (login)
            $users = new users();
            $users->login();
            break;
        case 6:
            //verifie si le user existe (login)
            $users = new users();
            $users->login_verify();
            break;
        case 7:
            $users = new users();
            $users->register();
            break;
        case 8:
            $users = new users();
            $users->register_verify();
            break;
        case 9:
            $users = new users();
            $users->logout();
            break;
        case 50:
            // page de download
            $compte = view_count("download_count.txt");
            $page_data = [
                'titre' => 'Telechargement-' . config::COMPANY_NAME,
                'desc' => 'fichier que vous voulez telecharger',
                'contenu' => '<h1>Selectionnier le fichier</h1>
                <p>
                <a href="index.php?op=51">un_fichier.pdf</a><br>
                <a href="index.php?op=52">texte_abc.txt</a><br>
                </p>
                Nb de vue =' . $compte
            ];
            webpage::render($page_data);
            break;

        case 51:
            //telecharger un fichier.pdf

            // le type de fichier, dans ce cas PDF, voir lien ci-dessous pour autres types
            header('Content-Type: application/pdf');
            // le nom du fichier sera un_fichier.pdf, navigateur peut demander permission
            header('Content-Disposition: attachment; filename="un_fichier.pdf"');
            // ok envoyer le fichier, lire et envoyer directement avec readfile()
            readfile('un_fichier.pdf');
            die();
            // rediriger vers une autre page

            break;

        case 52:
            //telecharger un fichier texte_abc.txt

            // le type de fichier, dans ce cas TXT, voir lien ci-dessous pour autres types
            header('Content-Type: text/plain');
            // le nom du fichier sera un_fichier.pdf, navigateur peut demander permission
            header('Content-Disposition: attachment; filename="texte.txt"');
            // ok envoyer le fichier, lire et envoyer directement avec readfile()
            readfile('texte_abc.txt');
            die();
            break;

        case 53:
            // rediriger vers une autre page
            header('location:http://www.canadiantire.ca');
            die();
            break;
        case 200:
            $customers = new customers();
            $customers->list();
            break;
        case 250:
            $customers = new customers();
            $customers->listJSON();
            break;
            //Product-Line
        case 700:
            //print list;

            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->list(700, 750, 752, 753, 754, product_line::check_order_by_request());
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        case 750:
            //add a product line
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->add(751);
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }

            break;
        case 751:
            //verify if product line can be add
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->add_verify();
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        case 752:
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->delete();
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        case 753:
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->view();
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        case 754:
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->edit();
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        case 755:
            if (users::isLogAsAdmin()) {
                $product_line = new product_line();
                $product_line->edit_verify();
            } else {
                crash('403', 'Acces interdit, Veuillez vous connecter.');
            }
            break;
        default:
            //http_response_code(400);
            crash(400, 'operation non supporter');
            break;
    }
}

//demarer le proggramme
main();
