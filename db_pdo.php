<?php

class db_pdo
{
    const DB_SERVER_TYPE = 'mysql';
    const DB_HOST = '127.0.0.1';
    const DB_PORT = '3306';
    const DB_NAME = 'classic_models';
    const DB_CHARSET = 'utf8mb4';
    const DB_USERNAME = 'classic_models_web_site';
    const DB_PASSWORD = '12345678';

    const OPTIONS = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO(
                self::DB_SERVER_TYPE . ':host=' . self::DB_HOST . ';port=' . self::DB_PORT . ';dbname=' . self::DB_NAME . ';charset=' . self::DB_CHARSET,
                self::DB_USERNAME,
                self::DB_PASSWORD,
                self::OPTIONS
            );
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur de connexion a la BD');
            die("Erreur connexion base de données : " . $e->getMessage());
        }
    }

    public function disconnect()
    {
        $this->connection = null;
    }

    public function query($sql)
    {
        try {
            $results = $this->connection->query($sql);
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur requete SQL');
            die("Erreur requetes pays: " . $e->getMessage());
        }
        return $results;  //retourne objet de type PDOStatement
    }

    public function query_select($sql)
    {
        try {
            $results = $this->connection->query($sql)->fetchAll();
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur requete SQL');
            die("Erreur requetes pays: " . $e->getMessage());
        }
        return $results;
    }

    public function query_params($sql, $params)
    {
        try {
            $statement = $this->connection->prepare($sql);
            $result = $statement->execute($params);
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur requete SQL');
            die("Erreur requetes pays: " . $e->getMessage());
        }
        return $result;  //retourne objet de type PDOStatement
    }

    public function query_select_params($sql, $params)
    {
        try {
            $statement = $this->connection->prepare($sql);
            $statement->execute($params);
            $result = $statement->fetchAll();
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur requete SQL');
            die("Erreur requetes pays: " . $e->getMessage());
        }
        return $result;  //retourne objet de type PDOStatement
    }
}
