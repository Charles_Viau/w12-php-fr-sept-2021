<?php
class product_line
{
    public function list($op_number, $op_add, $op_delete, $op_view, $edit_op, $order_by)
    {
        $request = self::make_sql_request($order_by);
        $content = "<div class='container-fluid'>
                        <div>
                            <h2>Mes Categories de Produits</h2>
                            <p>Nombre de Resultats : " . count($request) . "</p>
                        </div>
                        <div>
                            " . tools::create_html_basic_research_form($op_number) . "
                            <span>Ajouter une categorie : </span>
                            " . tools::create_add_button($op_add) . "
                        </div>
                        <div>
                        " . tools::display_data2($request, $op_delete, $op_view, $edit_op, 'Categorie') . "
                        </div>
                    </div>";

        webpage::render(webpage::create_page_data('Product Line List', 'Product Line List Research', $content));
    }
    public function add($op_add_verify, $msg = "")
    {
        $content = $msg;
        $content .=
            webpage::render(webpage::create_page_data('Add product line', 'Add product line to the database', self::create_form($op_add_verify, 'Ajouter une Categorie')));
        die();
    }

    private static function create_form($op, $title)
    {
        $info = "";

        if ($op == 753 || $op == 755) {
            $DB = new db_pdo();
            $info = $DB->query_select_params('SELECT * FROM productlines WHERE productLine = ?', [$_REQUEST['name']]);
        }
        $html =
            "<divclass='container-fluid'>
                <div class ='container'>
                    <div class='d-flex justify-content-around'>
                        <h2 class='p-2'>$title</h2>
                    </div>
                    <form action='index.php' method='POST'>
                        <input type='hidden' name='op' value= $op>";
        if ($op == 755) {
            $html .= "<input type='hidden' name='name' value='" . $info[0]['productLine'] . ">";
        }
        $html .= "<div class='form-group'>
                            <label for='product-line-name'>Product Line Name : </label>
                            <input type='text' id='product-line-name' name='product-line-name' required maxlength='100' class='form-control' ";
        if ($op == 753 || $op == 755) {
            $html .= 'value="' . $info[0]['productLine'] . '"';
        }
        if ($op == 753) {
            $html .= ' disabled';
        }
        $html .= ">
                        </div>
                        <div class='form-group'>
                            <label for='description'>Description : </label>
                            <textarea id='description' name='description' rows='4' cols='50' maxlength='500' class='form-control'";
        if ($op == 753) {
            $html .= ' disabled';
        }
        $html .= ">";

        if ($op == 753 || $op == 755) {
            $html .= $info[0]['textDescription'];
        }

        $html .= "</textarea>
                        </div>
                        <div class='form-group d-flex justify-content-around'>
                            <input type='submit' value='SUBMIT' class='btn btn-primary p-2'";
        if ($op == 753) {
            $html .= ' disabled';
        }
        $html .= ">
                        </div>
                    </form>
                </div>
            </div>";
        return $html;
    }

    public static function check_order_by_request()
    {
        return (isset($_REQUEST['order_by'])) ? $_REQUEST['order_by'] : 'productLine';
    }

    public static function check_search_by_request()
    {
        return (isset($_REQUEST['research_text'])) ? $_REQUEST['research_text'] : "";
    }

    public static function make_sql_request($order_by)
    {
        $DB = new db_pdo();
        if (isset($_POST['research_text']) && $_POST['research_text'] != '') {
            $list = $DB->query_select_params("SELECT productlines.productLine AS Categorie,textDescription AS Description,
                                                COUNT(products.productCode) AS 'Nombre de Produit' FROM productlines
                                                LEFT JOIN products ON products.productLine = productlines.productLine
                                                WHERE productlines.productLine = ?
                                                GROUP BY productlines.productLine
                                                ORDER BY productlines.$order_by", [$_REQUEST['research_text']]);
        } else {
            $list = $DB->query_select("SELECT productlines.productLine AS Categorie, textDescription AS Description,
                                        COUNT(products.productCode) AS 'Nombre de Produit'
                                        FROM productlines
                                        LEFT JOIN products ON products.productLine = productlines.productLine
                                        GROUP BY productlines.productLine
                                        ORDER BY productlines.$order_by");
        }
        return $list;
    }

    public function add_verify()
    {
        $err_msg = '';
        $err_msg .= tools::check_input('product-line-name', 100);
        $err_msg .= tools::isUnique('productlines', 'productLine', $_REQUEST['product-line-name']);
        $err_msg .= tools::check_input('description', 2000);

        if ($err_msg != '') {
            $this->add(751, $err_msg);
        } else {
            $DB = new db_pdo();
            $DB->query_params('INSERT INTO productlines (productLine, textDescription) VALUES (?, ?)', [$_REQUEST['product-line-name'], $_REQUEST['description']]);
            header('Location:index.php?op=700');
            die();
        }
    }

    public function delete()
    {
        tools::delete_request('productlines', 'productLine', $_REQUEST['name']);
        header('Location:index.php?op=700');
        die();
    }

    public function view()
    {
        webpage::render(webpage::create_page_data('View product line', 'View product line info in database', self::create_form(753, 'Information')));
        die();
    }

    public function edit($msg = "")
    {
        webpage::render(webpage::create_page_data('View product line', 'View product line info in database', $msg .= self::create_form(755, 'Modifier les Informations')));
        die();
    }

    public function edit_verify()
    {
        $err_msg = '';
        $err_msg .= tools::check_input('product-line-name', 100);
        $err_msg .= tools::isUnique('productlines', 'productLine', $_REQUEST['product-line-name']);
        $err_msg .= tools::check_input('description', 2000);

        if ($err_msg != '') {
            $this->edit($err_msg);
        } else {
            $DB = new db_pdo();
            $DB->query_params('UPDATE productlines SET productLine = ?, textDescription = ? WHERE productLine = ?', [$_REQUEST['product-line-name'], $_REQUEST['description'], $_REQUEST['name']]);
            header('Location:index.php?op=700');
            die();
        }
    }
}
