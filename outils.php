<?php
if (!isset($index_loaded)) {
    header('HTTP/1.0 403 acces direct a ce fichier est interdit');
    die('acces direct a ce fichier est interdis outil.php');
}

class tools
{
    /**
     * Create a select box input element.
     * @param array $table select's options list.
     * @param string $name name attribute of the select.
     * @param bool $required Optional - False by default, Determine if the user MUST choose an option.
     * @param string $selected  Optional - value of the option that will be already selected.
     * @return string return an HTML string
     */
    public static function create_html_select($table, $name, $required = false, $selected = '')
    {
        $select = '';
        if (!$required) $select .= '<select name=\'' . $name . '\'>';
        else {
            $select .= '<select name=\'' . $name . '\' required>';
        }
        $select .= '<option value=\'\' disabled selected hidden> Select an Option</option>';
        foreach ($table as $row) {
            if ($selected == $row['code']) $select .= '<option value=\'' . $row['nom'] . '\' selected>' . $row['nom'] . '</option>';
            else $select .= '<option value=\'' . $row['code'] . '\'>' . $row['nom'] . '</option>';
        }
        $select .= '</select>';

        return $select;
    }
    /**
     * Check uploaded file contains a valid image
     * extension must be: .jpg , .JPG , .gif ou .png.
     *
     * @param string $file_input the file input name on the HTML form
     * @param int    $Max_Size   maximum file size, default 500kb
     *
     * @return 'OK' or error message
     */
    public static function Picture_Uploaded_Is_Valid($file_input, $Max_Size = 500000)
    {
        //Form must have <form enctype="multipart/form-data" ..
        //otherwise $_FILE is undefined
        // $file_input is the file input name on the HTML form
        if (!isset($_FILES[$file_input])) {
            return 'No image uploaded';
        }

        //check for upload error
        if ($_FILES[$file_input]['error'] != UPLOAD_ERR_OK) {
            return 'Error picture upload: code=' . $_FILES[$file_input]['error'];
        }

        // Check image size
        if ($_FILES[$file_input]['size'] > $Max_Size) {
            return 'Image too big, max file size is ' . $Max_Size . ' Kb';
        }

        // Check that file actually contains an image
        $check = getimagesize($_FILES[$file_input]['tmp_name']);
        if ($check === false) {
            return 'This file is not an image';
        }

        // Check extension is jpg,JPG,gif,png
        $filePathArray = pathinfo($_FILES[$file_input]['name']);
        $fileExtension = $filePathArray['extension'];
        if ($fileExtension != 'jpg' && $fileExtension != 'JPG' && $fileExtension != 'gif' && $fileExtension != 'png') {
            return 'Invalid image file type, valid extensions are: .jpg .JPG .gif .png';
        }

        return 'OK';
    }

    /**
     *  Function to save uploaded image in a target directory
     *  (and display image for testing).
     *
     *  @param string $file_input the file input name on the HTML form
     *  @param string $target_dir the directory where to save picture
     *
     *  @return string OK or error message
     */
    public static function Picture_Uploaded_Save_File($file_input, $target_dir)
    {
        $message = self::Picture_Uploaded_Is_Valid($file_input); // voir fonction
        if ($message === 'OK') {
            // Check that no file with the same name already exists
            // in the target directory
            $target_file = $target_dir . "/" . basename($_FILES[$file_input]['name']);
            if (file_exists($target_file)) {
                return 'This file already exists';
            }

            // Create the file with move_uploaded_file()
            if (move_uploaded_file($_FILES[$file_input]['tmp_name'], $target_file)) {
                // ALL OK
                //display image for testing, comment next line when done
                echo '<img src="' . $target_file . '">';

                return '';
            } else {
                return 'Error in move_upload_file';
            }
        } else {
            // upload error, invalid image or file too big
            return $message;
        }
    }

    /**
     * Return the image MIME type (content-type).
     *
     * @param string $file_input the file name input on the form
     *
     * @return string the MIME type for example 'image/jpeg' or 'ERROR'
     */
    public static function Picture_Uploaded_Mime_Type($file_input)
    {
        // Attention: Utiliser function Photo_Uploaded_Is_Valid() avant !
        // On assume ici que la photo est valide
        $filePathArray = pathinfo($_FILES[$file_input]['name']);
        $fileExtension = $filePathArray['extension'];
        switch ($fileExtension) {
            case 'jpg':
            case 'JPG':
                return 'image/jpeg';

            case 'gif':
            case 'GIF':
                return 'image/gif';

            case 'png':
            case 'PNG':
                return 'image/png';
        }

        return 'ERROR';
    }

    /**
     * fonction pour convertir image en base64 mettre dans base de données.
     *
     * @param string $file_input the file name input on the form
     */
    public static function Picture_Uploaded_base64($file_input)
    {
        $message = self::Picture_Uploaded_Is_Valid($file_input); // voir fonction
        if ($message !== 'OK') {
            return $message;
        }
        $image = file_get_contents($_FILES[$file_input]['tmp_name']);

        // convertis image en base64
        // https://www.php.net/manual/en/function.base64-encode.php
        $image_base64 = base64_encode($image);

        return $image_base64;
    }

    /*function Picture_Save_BLOB($file_input)
    {
        $message = self::Picture_Uploaded_Is_Valid($file_input);
        if ($message === 'OK') {
            $mime_type = Photo_Uploaded_Mime_Type(); // voir function précédente
            if ($mime_type != 'ERROR') {
                $image = file_get_contents($_FILES['un_fichier']['tmp_name']);

                // convertis image en base64
                $image_base64 = base64_encode($image);
                // insertion dans base de donnée

            } else {
                echo 'Erreur ce type de fichier n\'est pas supporté';
            }
        } else {
            echo $message;
        }
    }
*/



    public static function create_html_basic_research_form($op_number)
    {
        $html = "<form action='index.php?op= $op_number' method='POST'>";
        $html .= "<label for='text'>Recherche :  </label>";
        $html .= "<input type='text' name='research_text' placeholder='Type research here...'>";
        $html .= "<input type='submit' value='Research'>";
        $html .= "</form>";
        return $html;
    }

    public static function make_sql_request($table, $search_by = "", $order_by = "")
    {
        $DB = new db_pdo();
        if (isset($_POST['research_text']) && $_POST['research_text'] != '') {
            $list = $DB->query_select_params("SELECT * FROM $table WHERE $search_by = ? ORDER BY $order_by ", [$_POST['research_text']]);
        } else {
            $list = $DB->query_select("SELECT * FROM $table ORDER BY $order_by");
        }
        return $list;
    }

    public static function display_data($data)
    {
        $output = "<table class='table'>";
        foreach ($data as $key => $row) {
            $output .= '<tr>';
            foreach ($row as $k => $v) {
                if ($key === 0) {
                    $output .= '<th>' . $k . '</th>';
                } else {
                    $output .= '<td>' . $v . '</td>';
                }
            }
            $output .= '</tr>';
        }
        $output .= '</table>';
        return $output;
    }
    public static function display_data2($data, $delete_op, $view_op, $edit_op, $specifics)
    {
        $output = "<table class='table'><thead><tr>";
        foreach ($data as $key => $row) {
            foreach ($row as $k => $v) {
                if ($key === 0) {
                    $output .= '<th>' . $k . '</th>';
                } else continue;
            }
        }
        $output .= '<th>Actions</th></tr></thead><tbody>';
        foreach ($data as $key => $row) {
            $output .= '<tr>';
            foreach ($row as $k => $v) {
                $output .= '<td>' . $v . '</td>';
            }
            $output .= '<td>
                        <div>
                            <a href="index.php?op=' . $view_op . '&name=' . $row[$specifics] . '" class="link-info">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
                                    <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
                                    <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
                                </svg>
                            </a>
                        </div>
                        <div>
                            <a href="index.php?op=' . $edit_op . '&name=' . $row[$specifics] . '" class="link-warning">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                </svg>
                            </a>
                        </div>
                        <div>
                            <a href="index.php?op=' . $delete_op . '&name=' . $row[$specifics] . '" class="link-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </a>
                        </div>
                        </td></tr>';
        }
        $output .= '</tbody></table>';
        return $output;
    }

    public static function create_add_button($op_add)
    {
        return "<a href='index.php?op=$op_add'>
                                <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-square' viewBox='0 0 16 16'>
                                    <path d='M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z'/>
                                    <path d='M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z'/>
                                </svg>
                            </a>";
    }

    public static function check_input($input_name, $length, $filter = "")
    {
        $err_msg = "";

        if (!isset($_REQUEST["$input_name"])) {
            $err_msg .= "$input_name not receive";
        } else {
            $input = $_REQUEST["$input_name"];
            if (strlen($input) > $length) {
                $err_msg .= "$input_name trop long, $length caracteres";
            }

            $input = htmlspecialchars($input);

            if ($filter != "" && !filter_var($input, $filter)) {
                $err_msg .= "$input_name invalide";
            }
        }

        return $err_msg;
    }

    public static function isUnique($table, $where_column, $where_specifics)
    {
        $DB = new db_pdo();
        if (count($DB->query_select_params("SELECT * FROM $table WHERE $where_column = ?", [$where_specifics])) < 1) {
            return "";
        } else {
            return "$where_specifics already exists";
        }
    }

    public static function delete_request($table, $column_name, $specific)
    {
        $DB = new db_pdo();
        $DB->query_params("DELETE FROM $table WHERE $column_name = ?", [$specific]);
    }
}
function crash($code, $msg)
{
    //todo enregistrer lerreur dans log/erreurs.log

    //TODO envoyer un email a ladmin
    header('http/1.0' . $code . '' . $msg);
    echo '<h1>Erreur ! </h1>';
    echo $msg;
    die();
}
/**
 * compte le nombre de vue dune page
 * @param $file_name nom du fichier ou le compte est enregistrer
 * @return int le nouveau compte
 */

function view_count($file_name)
{
    if (file_exists($file_name)) {
        $compte = file_get_contents($file_name);
    } else {
        $compte = 0;
    }
    $compte = file_get_contents($file_name);
    $compte++;
    file_put_contents($file_name, $compte);
    return $compte;
}
