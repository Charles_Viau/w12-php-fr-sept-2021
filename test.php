<!DOCTYPE html>
<html lang="fr">

<head>
    <title>
        test jour 1
    </title>
</head>

<body>
    <?php

    $unClient = [
        'id' => 34,
        'nom' => 'Charles',
        'pays' => 'Canada'
    ];
    /**
     * SayHi($nom, $id) affiche Bonjour suivi du nom du client et de son id.
     *
     *  @param string $nom nom a afficher
     *  @param int $id identifiant unique
     */
    function SayHi($nom, $id)
    {
        echo 'Bonjour ' . $nom,
        '<br/> Votre id : <b>' . $id, '</b><br/>';
    }
    /**
     * PrintTime() affiche la date
     */
    function PrintTime()
    {
        echo date('d/m/Y h/i/s A');
    }

    echo SayHi($unClient['nom'], $unClient['id']);
    PrintTime();
    phpinfo();
    ?>
</body>

</html>