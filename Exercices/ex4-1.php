<?php

function showTitle($title)
{
    echo "<h2>&#9830; $title</h2>";
    echo '<hr/>';
}

$phrase = 'Une phrase qui doit être affichée en mettant un mot par ligne';

showTitle('Exercice 1 nombre de caractères avec strlen()');
echo mb_strlen($phrase);

showTitle('Exercice 2 nombre de mots avec str_word_count()');

function str_word_count_real($phrase)
{
    return count(explode(' ', $phrase));
}

echo str_word_count_real($phrase);

showTitle('Exercice 3 en majuscule avec strtoupper()');
echo strtoupper($phrase);

showTitle('Exercice 4 première lettre de chaque mot en majuscule avec ucwords()');
echo ucwords($phrase);

showTitle('Exercice 5: un mot par ligne avec explode() et boucle foreach');

foreach (explode(' ', $phrase) as $word) {
    echo $word . '<br/>';
};

showTitle('Exercice 6: inverser un tableau avec array_reverse()');

foreach (array_reverse(explode(' ', $phrase)) as $word) {
    echo $word . '<br/>';
};

showTitle('Exercice 7 nombre de caractères sans les espaces avec substr_count()');

echo mb_strlen($phrase) - substr_count($phrase, ' ');

showTitle('Exercice 8 changer a pour b, c pour d, e pour f avec strtr()');

$replace = ['a' => 'b', 'c' => 'd', 'e' => 'f'];
echo strtr($phrase, $replace);
