<?php
function showTitle($title)
{
    echo "<h2>&#9830; $title</h2>";
    echo '<hr/>';
}

// reference https://www.php.net/manual/fr/function.date.php
// formatage de la date https://www.php.net/manual/fr/datetime.format.php

showTitle('Exercise 00 timestamp actuel, nombre de secondes depuis 1er janvier 1970 (votre réponse sera différente de celle ci-dessous)');

echo time();

showTitle('Exercise 01 Créer le timestamp pour 20h:25min:10s le 10 janvier 2019 avec 2 méthodes: mktime() et strtotime()');

$date = strtotime('10 January 2019 20 hours 25 minutes 10 seconds');
mktime(20, 25, 10, 1, 10, 2019);

// Les exrcises suivants doivent afficher les résultats pour le 10 janvier 2019
// à 20h:25min:10s , pas pour la date courante!

showTitle('Exercice 1 Date et heure au complet en format DATE_RFC2822');

echo date(DATE_RFC2822, $date);

showTitle('Exercice 2 Le jour du mois seulement');

echo date('d', $date);

showTitle('Exercice 3 Le mois en chiffre et en texte complet');

echo date('m F', $date);

showTitle('Exercice 4 Année seulement');

echo date('Y', $date);

showTitle('Exercice 5 Date en format 10,janvier,2019');

echo date('d, F, Y', $date);

showTitle('Exercice 6 Ajoute 1 mois à la date avec strtotime(), et affichage complet');



showTitle('Exercice 7 Nombre de jours écoulés entre le 10 janvier 2019 et le 31 décembre 1973');



showTitle('Exercice 8 Date en format jeudi, 10 janvier 2019');

echo date('D, d F Y', $date);
