<?php
/* Tableau des mois de l'année */
$mois_coul = [
    'Janvier' => 'blue',
    'Février' => 'white',
    'Mars' => 'Red',
    'Avril' => 'Yellow',
    'Mai' => 'Grey',
    'Juin' => 'Lime',
    'Juillet' => 'lightblue',
    'Août' => 'fuchsia',
    'Septembre' => 'lightgrey',
    'Octobre' => 'olive',
    'Novembre' => 'pink',
    'Décembre' => 'purple',
];
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <title>Tableau mois</title>
    <style>
        .container {
            width: 80%;
            border: 1px solid brown;
            margin: 10px;
            padding: 20px;
        }
    </style>
</head>

<body>
    <div id="wrapper">
        <header>
            <h1>Tableau mois</h1>
        </header>
        <main>
            <div class="container">
                <!--Première table -->
                <table>
                    <?php
                    foreach ($mois_coul as $mois => $coul) {
                        echo '<tr><td bgcolor=\'' . $coul . '\'>' . $mois . '</td></tr>';
                    }
                    ?>
                </table>
            </div>
            <div class="container">
                <!--Seconde table -->
                <table>
                    <?php
                    echo '<tr>';
                    foreach ($mois_coul as $mois => $coul) {
                        echo '<td bgcolor=\'' . $coul . '\'>' . $mois . '</td>';
                    }
                    echo '</tr>';
                    ?>
                </table>
            </div>
        </main>
        <footer>
        </footer>
    </div>
</body>

</html>