<?php

class db_pdo_ecole
{
    const DB_SERVER_TYPE = 'mysql';
    const DB_HOST = '127.0.0.1';
    const DB_PORT = '3306';
    const DB_NAME = 'ecole';
    const DB_CHARSET = 'utf8mb4';
    const DB_USERNAME = 'classic_models_web_site';
    const DB_PASSWORD = '12345678';

    const OPTIONS = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];

    private $connection;

    public function __construct()
    {
        try {
            $this->connection = new PDO(
                self::DB_SERVER_TYPE . ':host=' . self::DB_HOST . ';port=' . self::DB_PORT . ';dbname=' . self::DB_NAME . ';charset=' . self::DB_CHARSET,
                self::DB_USERNAME,
                self::DB_PASSWORD,
                self::OPTIONS
            );
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur de connexion a la BD');
            die("Erreur connexion base de données : " . $e->getMessage());
        }
    }

    public function disconnect()
    {
        $this->connection = null;
    }

    public function query_select($sql)
    {
        try {
            $results = $this->connection->query($sql)->fetchAll();
        } catch (PDOException $e) {
            header('HTTP/1.0 500 Erreur requete SQL');
            die("Erreur requetes pays: " . $e->getMessage());
        }
        return $results;
    }
}

function display_data($data)
{
    $output = '<table border=1>';
    foreach ($data as $key => $var) {
        $output .= '<tr>';
        foreach ($var as $k => $v) {
            if ($key === 0) {
                $output .= '<th>' . $k . '</th>';
            } else {
                $output .= '<td>' . $v . '</td>';
            }
        }
        $output .= '</tr>';
    }
    $output .= '</table>';
    return $output;
}

$DB = new db_pdo_ecole;
$sql = "SELECT * FROM profs ORDER BY AnneeAffectation DESC";
echo display_data($DB->query_select($sql));
