<?php
$Pays = ['CA' => 'Canada', 'US' => 'USA', 'MX' => 'Mexique', 'FR' => 'France', 'AU' => 'Autre'];
?>
<!DOCTYPE html>
<html lang="fr-CA">

<head>
    <meta charset="UTF-8">
    <title>Exercice 7-1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>


    <select name="pays">
        <?php
        foreach ($Pays as $key => $pays) {
            echo '<option value=\'' . $key . '\'>' . $pays . '</option>';
        }
        ?>
    </select>

    <!-- Modifier le code pour produire une fonction tableau_select_html($tableau,$nom_du_select,$selected=''){...}
        Cette fonction reçoit 3 paramètres:
        $tableau un tableau avec clé=>valeur comme le tableau des pays
        $nom_du_select le nom du champs SELECT dans le formulaire <select name=
        $selected='' paramètre optionel avec valeur par défaut '' une chaîne de caractère vide (2 apostrophes) Le code d'un élément sélectionné lors de l'affichage initial.
        Si aucune valeur est fournie en paramètre afficher le premier option dans la liste (c'est le comportement par défaut d'un select)
    -->
    <?php

    function tableau_select_html($tableau, $nom_du_select, $selected = '', $required = false)
    {
        if (!$required) echo '<select name=\'' . $nom_du_select . '\'>';
        else {
            echo '<select name=\'' . $nom_du_select . '\' required>';
        }
        echo '<option value=\'\' disabled selected hidden> Select an Option</option>';
        foreach ($tableau as $key => $tab) {
            if ($selected == $key) echo '<option value=\'' . $key . '\' selected>' . $tab . '</option>';
            else echo '<option value=\'' . $key . '\'>' . $tab . '</option>';
        }
        echo '</select>';
    }
    ?>
    </br>
    <?php
    $Provinces = ['QC' => 'Québec', 'ON' => 'Ontario', 'NB' => 'Nouveau-Brunswick', 'NS' => 'Nouvelle-Écosse', 'AB' => 'Alberta', 'MN' => 'Manitoba', 'SK' => 'Saskatchewan'];
    echo tableau_select_html($Provinces, 'province', 'NB');
    echo ' ';
    echo tableau_select_html($Pays, 'pays', 'FR');
    echo ' ';
    echo tableau_select_html($Pays, 'pays');
    ?>
</body>

</html>