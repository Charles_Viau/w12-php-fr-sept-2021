<?php
/* propriétés d'une page web */

$lang = 'fr-CA';
$title = 'ScooterElectrique.com - Acceuil';
$description = 'Le plus vaste de choix de scooters électrique à Montréal - Vente - Service - Pièces';
$author = 'Votre nom ici';
$icon = 'web_site_icon.jpg';
$content = 'bla bla bla bla bla ceci est le contenu de la page';
define('COMPANY_NAME', 'ScooterElectrique.com');
define('COMPANY_STREET_ADDRESS', '5340 St-Laurent');
define('COMPANY_CITY', 'Montréal');
define('COMPANY_PROVINCE', 'QC');
define('COMPANY_COUNTRY', 'Canada');
define('COMPANY_POSTAL_CODE', 'J0P 1T0');
define('COMPANY_PHONE', '514-111-1111');
define('COMPANY_EMAIL', 'email@email.com');

?>
<!DOCTYPE html>
<html lang="<?= $lang ?>">

<head>
    <meta charset="UTF-8">
    <title>ScooterElectrique.com - Acceuil</title>
    <meta name="DESCRIPTION" content="<?= $content ?>">
    <meta name="author" content="<?= $author ?>">
    <!-- web site icon -->
    <LINK REL="icon" href="<?= $icon ?>">

    <!--IMPORTANT pour responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

    <!-- PAGE HEADER -->
    <header>
        <h2 style="background-color:black;color:white;padding:10px">
            <?= $title ?>
        </h2>
    </header>

    <!-- BARRE DE NAVIGATION -->
    <nav style="background-color:blue;color:white;padding:10px">
        <a href='page.php'>Acceuil</a>
    </nav>

    <!-- CONTENT -->
    <?= $content ?>
    <!-- FOOTER -->
    <footer style="background-color:black;color:white;padding:10px">
        Exercice par <?= $author ?> &copy;
        <?php
        echo '<br/>' . COMPANY_NAME . ' ' . COMPANY_CITY . ' ' . COMPANY_COUNTRY;
        ?>
    </footer>
    </div>
</body>

</html>