<?php
$produits = [
    [
        'nom' => 'tondeuse',
        'no' => 'td1234',
        'prix' => 199.99,
        'poids' => 50,
    ],
    [
        'nom' => 'râteau',
        'no' => 'ra9xfg',
        'prix' => 19.99,
        'poids' => 5,
    ],
    [
        'nom' => 'pelle',
        'no' => 'pe4532',
        'prix' => 19.99,
        'poids' => 5,
    ],
];

echo '<table border= 1><tr><th>nom</th><th>no</th><th>prix</th><th>poids</th></tr>';
foreach ($produits as $produit) {
    echo '<tr><td>' . $produit['nom'] . '</td><td>' . $produit['no'] . '</td><td>' . $produit['prix'] . '</td><td>' . $produit['poids'] . '</td>';
}
