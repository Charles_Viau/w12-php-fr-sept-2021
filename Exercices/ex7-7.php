<?php

function showTitle($title)
{
    echo "<br/><br/><b>&#9830; $title</b><br/>";
    echo '<hr/>';
}

$users = [
    [
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ],
    [
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ],
    [
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ],
    [
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    ],
];

showTitle("Exercice 1 Afficher les prénoms sans effectué d'itération avec array_column() et implode()");

echo implode(', ', array_column($users, 'first_name'));

showTitle('Exercice 2 Afficher la valeur de l\'élément différent entre les deux collections $firstArray et $secondArray sans exécuter d\'itération avec array_diff()');

$firstArray = ['a' => 'auto', 'b' => 'moto', 'c' => 'avion'];
$secondArray = ['a' => 'auto', 'b' => 'moto'];

echo var_dump(array_diff($firstArray, $secondArray));

showTitle('Exercice 3 Inverser les clefs et les valeurs de $firstArray sans exécuter d\'itération, utilisez plutôt la fonction array_flip()');

echo var_dump(array_flip($firstArray));
