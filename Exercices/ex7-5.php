<?php
$Products = ['Produit A' => 12, 'Produit B' => 23, 'Produit C' => 2];
//$Products = [];// panier vide
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercice 4-4 panier</title>
    <style>
        table {
            width: 200px;
            border: 2px solid black;
        }

        table td {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <h1>Liste de produits</h1>
    <?php
    if (count($Products) > 0) {
        echo '<p>Votre panier contient ' . count($Products) . ' produits.<p>';
        echo '<table><thead><tr><th>Nom</th><th>Prix</th></tr></thead><tbody>';
        foreach ($Products as $name => $price) {
            echo '<tr><td>' . $name . '</td><td>' . $price . '</td>';
        }
        echo '</tbody></table>';
    } else {
        echo '<p>Votre panier contient aucun produit.</p>';
        echo '<a href="">Consultez la liste des spéciaux de la semaine</a>';
    }
    ?>
</body>

</html>