<?php

$Candidats = [
    ['Pierre', 22, '123 rue A', 'aa@ser.com', ['programmation' => 5, 'enseignement' => 2]],
    ['Julie', 65, '123 rue B', 'bb@ser.com', ['électronique' => 46]],
    ['Martin', 45, '123 rue C', 'cc@ser.com', ['programmation' => 21, 'enseignement' => 1]],
    ['Mélanie', 41, '123 rue D', 'dd@ser.com', ['soudure' => 12, 'nutrition' => 6, 'restoration' => 1]]
];

// Couleur de fond noire lorsque l'âge est exactement égal à la référence, vert lorsque supérieur et bleu lorsque inférieur
const AGE_REFERENCE = 45;

// Couleur de fond jaune lorsque le nombre d'années d'expérience est égal ou supérieur a MINIMUM_EXPERIENCE
const MINIMUM_EXPERIENCE = 5;

?>

<?php
function define_age_style($age)
{
    if ($age < AGE_REFERENCE) {
        return 'age-under';
    } else if ($age > AGE_REFERENCE) {
        return 'age-over';
    } else if ($age == AGE_REFERENCE) {
        return 'age-reference';
    }
}

function define_job_experience($year)
{
    if ($year >= MINIMUM_EXPERIENCE) {
        return 'experience-valid';
    } else return 'experience-invalid';
}

function get_average_age($array)
{
    return array_sum(array_column($array, 1)) / count($array);
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Exercice 4-3 - Tableau</title>

    <style>
        table,
        td,
        th {
            border: 1px solid black;
            margin: auto;
        }

        ul {
            list-style-type: none;
            padding: 5px;
        }

        .age-reference {
            background-color: black;
            color: white;
        }

        .age-over {
            background-color: green;
            color: white;
        }

        .age-under {
            background-color: blue;
            color: white;
        }

        .experience-invalid {
            background-color: white;
            color: black;
        }

        .experience-valid {
            background-color: yellow;
            color: black;
            font-weight: bold;
        }
    </style>
</head>

<body>

    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Age</th>
                <th>Adresse</th>
                <th>Courriel</th>
                <th>Emplois</th>
            </tr>
        </thead>

        <tbody>
            <?php
            for ($i = 0; $i < count($Candidats); $i++) {
                echo '<tr>';
                for ($j = 0; $j < count($Candidats[$i]); $j++) {
                    echo '<td class="' . define_age_style($Candidats[$i][1]) . '">';
                    if (gettype($Candidats[$i][$j]) == 'array') {
                        echo '<ul>';
                        foreach ($Candidats[$i][$j] as $job => $experience) {
                            echo '<li class="' . define_job_experience($experience) . '">' . $job . ' : ' . $experience . '</li>';
                        }
                        echo '</ul>';
                    } else {
                        echo $Candidats[$i][$j];
                    }
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
        </tbody>

        <tfoot>
            <tr>
                <td>Moyenne</td>
                <td>
                    <?php
                    echo get_average_age($Candidats);
                    ?>
                </td>
                <td colspan="3"></td>
        </tfoot>
    </table>

</body>

</html>